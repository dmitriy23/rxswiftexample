//: Playground - noun: a place where people can play

import PlaygroundSupport

PlaygroundPage.current.needsIndefiniteExecution = true

import RxSwift

// Observable

example("just") {
    // Observable
    let observable = Observable.just("RxSwift") // just - создает последовательность
    
    // Observer
    observable.subscribe({ (event: Event<String>) in
        print(event)
    })
    
    /*
     /// Next element is produced.
     next(Element)
     
     /// Sequence terminated with an error.
     error(Swift.Error)
     
     /// Sequence completed successfully.
     completed
     */
}

example("of") {
    let observable = Observable.of(2, 34, 4563, 4, 4, 43)
    observable.subscribe {
        print($0)
    }
}

example("create") {
    let items = [45, 34, 5, 234, 5, 0]
    Observable.from(items).subscribe(onNext: { event in
        print(event)
    }, onError: { error in
        print(error)
    }, onCompleted: { 
        print("Completed")
    }, onDisposed: { 
        print("Disposed")
    })
    
}

// Dispose

example("dispose") {
    let sequence = [4, 56, 23, 1000, 3]
    let subscription = Observable.from(sequence)
    subscription.subscribe({ event in
        print(event)
    }).dispose() // принудительная отписка
}

example("disposeBag") {
    let disposeBag = DisposeBag()
    let sequence = [6, 89, 34, 1, 0]
    let subscription = Observable.from(sequence)
    subscription.subscribe({ event in
        print(event)
    }).addDisposableTo(disposeBag) // корректная отписка
}

example("takeUntil") {
    let stopSequence = Observable.just(1).delay(2, scheduler: MainScheduler.instance)
    let sequence = [6, 89, 34, 1, 0]
    let subscription = Observable.from(sequence).takeUntil(stopSequence)
    subscription.subscribe { print($0) }
}

// Operators

example("filter") {
    let sequence = Observable.of(1, 45, 67, 63, 3, 9, 10, 45).filter { $0 >= 10 }
    sequence.subscribe { print($0) }
}

example("map") {
    let sequence = Observable.of(1, 2, 3, 4, 5).map { $0 * 10 }
    sequence.subscribe { print($0) }
}

example("merge") {
    let sequence1 = Observable.of(1, 2, 3, 4, 5)
    let sequence2 = Observable.of(6, 7, 8, 9, 10)
    let mergedSequence = Observable.of(sequence1, sequence2).merge()
    mergedSequence.subscribe { print($0) }
}

// Queue

example("without observeOn") {
    _ = Observable.of(1, 2, 3)
        .subscribe(onNext: {
            print("\(Thread.current): ", $0)
        }, onError: nil, onCompleted: {
            print("Completed")
        }, onDisposed: nil)
}

/*
example("observeOn") {
    _ = Observable.of(1, 2, 3)
        .observeOn(ConcurrentDispatchQueueScheduler(globalConcurrentQueueQOS: .background))
        .subscribe(onNext: {
            print("\(Thread.current): ", $0)
        }, onError: nil, onCompleted: {
            print("Completed")
        }, onDisposed: nil)
}
*/

example("queue") {
    
    let queue1 = DispatchQueue.global(qos: .default)
    let queue2 = DispatchQueue.global(qos: .default)
    
    print("Init Thread: \(Thread.current)")
    _ = Observable<Int>.create({ (observer) -> Disposable in
        print("Observable thread: \(Thread.current)")
        
        observer.on(.next(1))
        observer.on(.next(2))
        observer.on(.next(3))
        
        return Disposables.create()
    })
        .subscribeOn(SerialDispatchQueueScheduler(internalSerialQueueName: "queue1"))
        .observeOn(SerialDispatchQueueScheduler(internalSerialQueueName: "queue2"))
        
        .subscribe(onNext: {
            print("Observable thread: \(Thread.current)", $0)
        })
}

// Side effect

example("sideEffect") {
    
    //-------1---2---3---|---> Source Observable
    //       |   |   |
    //  doOn{_ in action() } - Operator
    //       |   |   |
    //       ↓   ↓   ↓
    //-------1---2---3---|---> Result Observable
    
    //doOnNext
    //doOnError
    //doOnComplete
    
    let disposeBag = DisposeBag()
    
    let sequence = [0, 32, 100, 300]
    let tempSequence = Observable.from(sequence)
    
    tempSequence.do(onNext: {
        print("\($0)F = ", terminator: "")
    }).map({
        Double($0 - 32) * 5/9.0
    }).subscribe(onNext: {
        print(String(format: "%.1f", $0))
    }).addDisposableTo(disposeBag)
    
}

// Subjects

example("publishSubject") {
    let disposablebag = DisposeBag()
    
    let subject = PublishSubject<String>()
    
    subject.subscribe {
        print("Subscription First: ", $0)
        }.addDisposableTo(disposablebag)
    
    enum myError: Error {
        case Test
    }
    
    subject.on(.next("Hello"))
    //subject.onCompleted()
    //subject.onError(myError.Test)
    subject.onNext("RxSwift")
    
    subject.subscribe(onNext: {
        print("Subscription Second: ", $0)
    }).addDisposableTo(disposablebag)
    
    subject.onNext("Hi!")
    subject.onNext("My name is Kostya")
}


example("behaviorSubject") {
    let disposablebag = DisposeBag()
    let subject = BehaviorSubject(value: 1) //[1]
    
    _ = subject.subscribe(onNext: {
        print(#line, $0)
    }).addDisposableTo(disposablebag)
    
    subject.onNext(2) //[1,2]
    subject.onNext(3) //[1,2,3]
    
    _ = subject.map({ $0 + 2 }).subscribe(onNext: {
        print(#line, $0) //[3]
    }).addDisposableTo(disposablebag)
}



example("replaySubject") {
    let disposablebag = DisposeBag()
    /*
    let subject = ReplaySubject<String>.create(bufferSize: 1)
    
    subject.subscribe {
        print("First subscription: ", $0)
        }.addDisposableTo(disposablebag)
    
    subject.onNext("a")
    subject.onNext("b")
    
    subject.subscribe {
        print("Second subscription: ", $0)
        }.addDisposableTo(disposablebag)
    
    subject.onNext("c")
    subject.onNext("d")
    */
    let subject = ReplaySubject<Int>.create(bufferSize: 3)
    
    subject.onNext(1)
    subject.onNext(2)
    subject.onNext(3)
    subject.onNext(4)
    
    subject.subscribe {
        print($0)
        }.addDisposableTo(disposablebag)
}

// Variables

example("variables") {
    let disposableBag = DisposeBag()
    
    let variable = Variable("A")
    
    variable.asObservable().subscribe(onNext: {
        print($0)
    }).addDisposableTo(disposableBag)
    
    variable.value = "B"
}
